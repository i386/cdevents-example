//
//  WHAppDelegate.h
//  cdeventexample
//
//  Created by James Dumay on 16/02/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WHAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
