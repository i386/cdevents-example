//
//  WHAppDelegate.m
//  cdeventexample
//
//  Created by James Dumay on 16/02/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHAppDelegate.h"

#import <CDEvents/CDEvents.h>

@interface WHAppDelegate ()

@property (strong) CDEvents *parentEvents;
@property (strong) CDEvents *childEvents;

@end

@implementation WHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSURL *parentURL = [[NSURL fileURLWithPath:NSHomeDirectory()] URLByAppendingPathComponent:@"test"];
    NSURL *childURL = [parentURL URLByAppendingPathComponent:@"child"];
    
    //Create all directories
    [[NSFileManager defaultManager] createDirectoryAtPath:[childURL path] withIntermediateDirectories:YES attributes:nil error:nil];
    
    _childEvents = [[CDEvents alloc] initWithURLs:@[childURL] block:^(CDEvents *watcher, CDEvent *event) {
        NSLog(@"child event");
    }];
    
    _parentEvents = [[CDEvents alloc] initWithURLs:@[parentURL]
                             block:^(CDEvents *watcher, CDEvent *event) {
                                 if (event.isCreated || event.isRemoved || event.isRenamed)
                                 {
                                     NSLog(@"parent event");
                                 }
                             }
                         onRunLoop:[NSRunLoop currentRunLoop]
              sinceEventIdentifier:kCDEventsSinceEventNow
              notificationLantency:CD_EVENTS_DEFAULT_NOTIFICATION_LATENCY
           ignoreEventsFromSubDirs:YES
                       excludeURLs:nil
               streamCreationFlags:kCDEventsDefaultEventStreamFlags];

    
    NSURL *parentfile = [parentURL URLByAppendingPathComponent:@"parent.txt" isDirectory:NO];
    [[NSFileManager defaultManager] createFileAtPath:[parentfile path] contents:nil attributes:nil];
    
    NSURL *childFile = [childURL URLByAppendingPathComponent:@"child.txt" isDirectory:NO];
    [[NSFileManager defaultManager] createFileAtPath:[childFile path] contents:nil attributes:nil];
}

@end
